<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Rules\MatchOldPassword;
use Dotenv\Exception\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        return view('users.admin.index', ['user'=> $user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user=Auth::user();
        return view('users.admin.edit', ['user'=> $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit_profile()
    {
        $user=Auth::user();
        return view('users.admin.edit', ['user'=> $user]);
    }

    public function update_profile(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255', 'unique:users,email,'.Auth::user()->id],
        ]);

        $path = public_path().'/uploads';
        if($request->file){
            $request->validate([
                'file'=>'mimes:jpeg,jpg,png,gif|max:10000' // max 10000kb
            ]);
            if (!file_exists($path)){
                Storage::makeDirectory($path);
            }
            $fileName = time().'.'.$request->file->extension();
            try {
                $request->file->move(public_path('uploads'), $fileName);
                $update=User::where('id', Auth::user()->id)->update([
                    'name'=>$request->name,
                    'email'=>$request->email,
                    'profile_picture'=>$fileName,
                ]);
                return redirect('/admin/home/edit')
                    ->withInput();
            }catch (\Exception $e){
                throw new ValidationException(['file'=>'Something went wrong.']);
            }
        }else{
            try {
                $update=User::where('id', Auth::user()->id)->update([
                    'name'=>$request->name,
                    'email'=>$request->email,
                ]);
                return redirect('/admin/home/edit')
                    ->withInput();
            }catch (\Exception $e){
                throw new ValidationException(['file'=>'Something went wrong.']);
            }
        }
    }

    public function update_password(Request $request){
        $request->validate([
            'old_password'=>['required', 'string', 'min:8', new MatchOldPassword],
            'password'=>['required', 'string', 'min:8'],
            'confirm_password'=>['required', 'string', 'min:8', 'same:password']
        ]);
        try {
            $update=User::where('id', Auth::user()->id)->update([
                'password'=>Hash::make($request->password),
            ]);
            return redirect('/admin/home/edit')
                ->withInput();
        }catch (\Exception $e){
            throw new ValidationException(['password'=>'Something went wrong.']);
//            $error = \Illuminate\Validation\ValidationException::withMessages([
//                'password' => ['Something went wrong.'],
//            ]);
//            throw $error;
        }
    }
}
