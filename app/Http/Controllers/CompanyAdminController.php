<?php

namespace App\Http\Controllers;

use App\Mail\InvitationMail;
use App\Models\Company;
use App\Models\Invitation;
use App\Models\User;
use App\Rules\IsRegisteredUser;
use App\Rules\MatchOldPassword;
use Dotenv\Exception\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class CompanyAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        $company=Company::where('admin_id', $user->id)->first();
        return view('users.company_admin.index', ['user'=> $user, 'company'=>$company]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.company_admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $users=$user->where('type', 'user')->get();
        return view('users.company_admin.users.index', ['users'=> $users]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit_profile(Company $company)
    {
        $user=Auth::user();
        $company_edit=$company->where('admin_id', $user->id)->first();
        return view('users.company_admin.edit', ['user'=> $user, 'company'=>$company_edit]);
    }

    public function update_profile(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255', 'unique:users,email,'.Auth::user()->id],
        ]);

        $path = public_path().'/uploads';
        if($request->file){
            $request->validate([
                'file'=>'mimes:jpeg,jpg,png,gif|max:10000' // max 10000kb
            ]);
            if (!file_exists($path)){
                Storage::makeDirectory($path);
            }
            $fileName = time().'.'.$request->file->extension();
            try {
                $request->file->move(public_path('uploads'), $fileName);
                $update=User::where('id', Auth::user()->id)->update([
                    'name'=>$request->name,
                    'email'=>$request->email,
                    'profile_picture'=>$fileName,
                ]);
                return redirect('/company-admin/home/edit')
                    ->withInput();
            }catch (\Exception $e){
                throw new ValidationException(['file'=>'Something went wrong.']);
            }
        }else{
            try {
                $update=User::where('id', Auth::user()->id)->update([
                    'name'=>$request->name,
                    'email'=>$request->email,
                ]);
                return redirect('/company-admin/home/edit')
                    ->withInput();
            }catch (\Exception $e){
                throw new ValidationException(['file'=>'Something went wrong.']);
            }
        }
    }

    public function update_password(Request $request){
        $request->validate([
            'old_password'=>['required', 'string', 'min:8', new MatchOldPassword],
            'password'=>['required', 'string', 'min:8'],
            'confirm_password'=>['required', 'string', 'min:8', 'same:password']
        ]);
        try {
            $update=User::where('id', Auth::user()->id)->update([
                'password'=>Hash::make($request->password),
            ]);
            return redirect('/company-admin/home/edit')
                ->withInput();
        }catch (\Exception $e){
            throw new ValidationException(['password'=>'Something went wrong.']);
//            $error = \Illuminate\Validation\ValidationException::withMessages([
//                'password' => ['Something went wrong.'],
//            ]);
//            throw $error;
        }
    }

    public function update_company(Request $request){
        $request->validate([
            'company_name' => ['required', 'string', 'max:255'],
            'company_address' => ['required', 'string', 'max:255'],
        ]);
        try {
            $update=Company::where('admin_id', Auth::user()->id)->update([
                'name'=>$request->company_name,
                'address'=>$request->company_address,
            ]);
            return redirect('/company-admin/home/edit')
                ->withInput();
        }catch (\Exception $e){
//            throw new ValidationException(['company_name'=>'Something went wrong.']);
            $error = \Illuminate\Validation\ValidationException::withMessages([
                'company_name' => ['Something went wrong.'],
            ]);
            throw $error;
        }
    }

    public function user_invitation(Request $request, Mailer $mailer){
        $request->validate([
            'email' => ['required', 'email', 'max:255', new IsRegisteredUser()],
        ]);
        $invitation_code='';
        $invitation=Invitation::where('email', $request->email)->first();
        $user=User::where('email', $request->email)->first();
        $company=Company::where('admin_id', Auth::user()->id)->first();
        if($user===null){
            if($invitation===null){
                $invitation_code=time();
                Invitation::create([
                    'email'=>$request->email,
                    'invitation_code'=>$invitation_code,
                    'company_id'=>$company->id,
                ]);
            }else{
                $invitation_code=$invitation->invitation_code;
            }
            $mailer
                ->to($request->email)
                ->send(new InvitationMail($invitation_code, $request->email));
        }
        return redirect()->back()->with('message', 'User Invited');
//        $email=$request->email;
//        Mail::send('users.user.email', ['email' => $email], function ($m) use ($email) {
//            $m->from(\auth()->user()->email, 'Your Application');
//
//            $m->to($email)->subject('Your Reminder!');
//        });
    }
}
