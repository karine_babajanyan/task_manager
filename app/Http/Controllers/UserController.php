<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Invitation;
use App\Models\User;
use App\Models\UserComainy;
use App\Models\UserCompany;
use App\Rules\MatchOldPassword;
use Carbon\Carbon;
use Dotenv\Exception\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function show_registration_form(Request $request){
        return view('users.user.register',['email'=>$request->email, 'code'=>$request->route('code')]);
    }

    public function registration_user(Request $request){
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $invitation=Invitation::where('email',$request->email)->where('invitation_code',$request->route('code'))->first();
        if ($invitation===null){
            $error = \Illuminate\Validation\ValidationException::withMessages([
                'email' => ['Email is not correct'],
            ]);
            throw $error;
        }else{
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'email_verified_at'=>Carbon::now()->toDateTimeString(),
                'type' => 'user',
            ]);
            if($user){
                UserCompany::create([
                    'user_id'=>$user->id,
                    'company_id'=>$invitation->company_id,
                ]);
            }

            $credentials = $request->only('email', 'password');
            if (Auth::attempt($credentials)) {
                return redirect()->intended('users.user.index')
                    ->withSuccess('Signed in');
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()){
            return view('users.user.index',['user'=> Auth::user()]);
        }
//        $user=User::
//        $this->guard()->login($request->user);
//        return view('users.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
//        dd(Auth::user());
        return view('users.user.edit',['user'=> Auth::user()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_profile(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255', 'unique:users,email,'.Auth::user()->id],
        ]);

        $path = public_path().'/uploads';
        if($request->file){
            $request->validate([
                'file'=>'mimes:jpeg,jpg,png,gif|max:10000' // max 10000kb
            ]);
            if (!file_exists($path)){
                Storage::makeDirectory($path);
            }
            $fileName = time().'.'.$request->file->extension();
            try {
                $request->file->move(public_path('uploads'), $fileName);
                $update=User::where('id', Auth::user()->id)->update([
                    'name'=>$request->name,
                    'email'=>$request->email,
                    'profile_picture'=>$fileName,
                ]);
                return redirect('/user/edit')
                    ->withInput();
            }catch (\Exception $e){
                throw new ValidationException(['file'=>'Something went wrong.']);
            }
        }else{
            try {
                $update=User::where('id', Auth::user()->id)->update([
                    'name'=>$request->name,
                    'email'=>$request->email,
                ]);
                return redirect('/user/edit')
                    ->withInput();
            }catch (\Exception $e){
                throw new ValidationException(['file'=>'Something went wrong.']);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_password(Request $request)
    {
        $request->validate([
            'old_password'=>['required', 'string', 'min:8', new MatchOldPassword],
            'password'=>['required', 'string', 'min:8'],
            'confirm_password'=>['required', 'string', 'min:8', 'same:password']
        ]);
        try {
            $update=User::where('id', Auth::user()->id)->update([
                'password'=>Hash::make($request->password),
            ]);
            return redirect('/user/edit')
                ->withInput();
        }catch (\Exception $e){
            throw new ValidationException(['password'=>'Something went wrong.']);
//            $error = \Illuminate\Validation\ValidationException::withMessages([
//                'password' => ['Something went wrong.'],
//            ]);
//            throw $error;
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }
}
