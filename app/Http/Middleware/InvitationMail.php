<?php

namespace App\Http\Middleware;

use App\Models\Invitation;
use Closure;
use Illuminate\Http\Request;

class InvitationMail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $invitation=Invitation::where('email', $request->email)->first();
        if($invitation===null){
                return response()->view('users.errors.error404');
        } else {
            if ($invitation->invitation_code !== $request->route('code')) {
                if(md5($request->route('code'))!==$request->route('hash_code')){
                    return response()->view('users.errors.error404');
                }
            }else{
                if(md5($request->route('code'))!==$request->route('hash_code')){
                    return response()->view('users.errors.error404');
                }
            }
        }
        return $next($request);
    }
}
