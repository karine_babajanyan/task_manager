<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvitationMail extends Mailable
{
    use Queueable, SerializesModels;
    private $code;
    private $email;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($code, $email)
    {
        $this->code=$code;
        $this->email=$email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('karine.babajanyan@willing-able.dev')
        ->view('users.user.email',['code'=>$this->code, 'email'=>$this->email]);
    }
}
