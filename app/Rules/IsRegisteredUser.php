<?php

namespace App\Rules;

use App\Models\Invitation;
use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class IsRegisteredUser implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $invitation = Invitation::where('email', $value)->where('invitation_email_verified_at', '!=', NULL)->first();
        $user=User::where('email', $value)->first();
        if ($invitation === null  && $user === null) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute is already registered.';
    }
}
