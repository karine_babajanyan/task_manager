$(document).ready(function() {

    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".file-upload").on('change', function(){
        readURL(this);
    });

    $(".upload-button").on('click', function() {
        $(".file-upload").click();
    });
    $('[data-widget="pushmenu"]').click(function () {
        if(screen.width>991){
            if($('.main-sidebar').width()===250){
                $('.container-fluid').css({'padding-left':(73.6+7.5)+'px'})
            }else {
                $('.container-fluid').css({'padding-left':(250+7.5)+'px'})
            }
        }
    })
    if(screen.width>=992){
        $('[data-widget="pushmenu"]').PushMenu("expand");
    }else{
        $('[data-widget="pushmenu"]').PushMenu("collapse");
    }
});
$( window ).resize(function() {
    if(screen.width===992){
        $('[data-widget="pushmenu"]').PushMenu("expand");
        $('.container-fluid').css({'padding-left':($('.main-sidebar').width()+7.5)+'px'})
    }else if(screen.width<992){
        $('[data-widget="pushmenu"]').PushMenu("collapse");
        $('.container-fluid').css({'padding-left':'7.5px'})
    }else{
        $('.container-fluid').css({'padding-left':($('.main-sidebar').width()+7.5)+'px'})
    }
});
