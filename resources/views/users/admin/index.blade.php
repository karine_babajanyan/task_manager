@extends('users.layouts.layout')

@section('content')
    <div class="main-body" style="width: 80%; margin: auto">
        <!-- /Breadcrumb -->

        <div class="row gutters-sm">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center">
                            {{--                                <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="Admin" class="rounded-circle" width="150">--}}
                            @if($user->profile_picture ==null)
                                <img class="profile-pic" src="https://cdn1.iconfinder.com/data/icons/random-115/24/person-512.png" width="150" />
                            @else
                                <img class="profile-pic" src='{{asset("uploads/$user->profile_picture")}}' width="150" />
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card mb-3">
                    <div class="card-header">
                        <h3 class="m-0 float-left">Personal Information</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Name</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                {{ $user->name }}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Email</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                {{ $user->email }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
