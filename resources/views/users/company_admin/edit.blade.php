@extends('users.layouts.layout')

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h3 class="m-0 float-left">Personal Information</h3>

{{--                    <!-- Button trigger modal -->--}}
{{--                    <button type="button" class="btn btn-light float-right" data-toggle="modal" data-target="#exampleModalCenter">--}}
{{--                        <strong>Edit Company</strong>--}}
{{--                    </button>--}}

{{--                    <!-- Modal -->--}}
{{--                    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">--}}
{{--                        <div class="modal-dialog modal-dialog-centered" role="document">--}}
{{--                            <div class="modal-content">--}}
{{--                                <div class="modal-header">--}}
{{--                                    <h5 class="modal-title" id="exampleModalLongTitle">Company Information</h5>--}}
{{--                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                                        <span aria-hidden="true">&times;</span>--}}
{{--                                    </button>--}}
{{--                                </div>--}}
{{--                                <form method="POST" action="{{ route('company.update') }}">--}}
{{--                                    <div class="modal-body">--}}
{{--                                        {{ csrf_field() }}--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label>Company Name</label>--}}
{{--                                            <input class="form-control" type="text" name="company_name" value="{{$company->name}}">--}}
{{--                                            @error('company_name')--}}
{{--                                            <span class="invalid-feedback" role="alert">--}}
{{--                                                        <strong>{{ $message }}</strong>--}}
{{--                                                    </span>--}}
{{--                                            @enderror--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label>Company Address</label>--}}
{{--                                            <input class="form-control" type="text" name="company_address" value="{{$company->address}}">--}}
{{--                                            @error('company_address')--}}
{{--                                            <span class="invalid-feedback" role="alert">--}}
{{--                                                        <strong>{{ $message }}</strong>--}}
{{--                                                    </span>--}}
{{--                                            @enderror--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="modal-footer">--}}
{{--                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
{{--                                        <button type="button" class="btn btn-info">Update Company</button>--}}
{{--                                    </div>--}}
{{--                                </form>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
                <div class="row">
                    <div class="col-6">
                        <form class="card-body" method="POST" action="{{ route('company-admin.home.update.profile') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-12 mb-3">
                                    <div class="avatar-wrapper @error('file') is-invalid @enderror">
                                        @if($user->profile_picture ==null)
                                            <img class="profile-pic" src="https://cdn1.iconfinder.com/data/icons/random-115/24/person-512.png" />
                                        @else
                                            <img class="profile-pic" src='{{asset("uploads/$user->profile_picture")}}' />
                                        @endif
                                        <div class="upload-button">
                                            <i class="fas fa-arrow-circle-up"></i>
                                        </div>
                                        {{--                                        <input id="file" type="file" class="file-upload" name="file">--}}
                                        <input type="file" name="file" class="form-control file-upload" id="file">
                                    </div>
                                    @error('file')
                                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Name</label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" required autocomplete="name" autofocus>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" required autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <button class="btn btn-info" type="submit">Update</button>
                            </div>
                        </form>
                    </div>

                    <div class="col-6">
                        <div class="card-body">
                            <h4 class="text-white text-white bg-dark d-inline-block p-1 rounded m-0"><strong>Change</strong> Password</h4>
                            <form class="card-body" method="POST" action="{{ route('company-admin.home.update.password') }}">
                                @csrf
                                <div class="form-group">
                                    <label>Current Password</label>
                                    <input id="old_password" type="password" class="form-control @error('old_password') is-invalid @enderror" name="old_password" required autocomplete="old_password" autofocus>

                                    {{--                                    <input class="form-control @error('old_password') is-invalid @enderror" type="password" name="old_password" placeholder="••••••">--}}
                                    @error('old_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>New Password</label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="password" autofocus>
                                    {{--                                    <input class="form-control @error('password') is-invalid @enderror" type="password" name="password" placeholder="••••••">--}}
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Confirm <span class="d-none d-xl-inline">Password</span></label>
                                    <input id="confirm_password" type="password" class="form-control @error('confirm_password') is-invalid @enderror" name="confirm_password" required autocomplete="confirm_password" autofocus>
                                    {{--                                    <input class="form-control @error('confirm_password') is-invalid @enderror" type="password" name="confirm_password" placeholder="••••••">--}}
                                    @error('confirm_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-info" type="submit">Update Password</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h3 class="m-0 float-left">Company Information</h3>
                </div>
                <div class="row">
                        <div class="col-6">
                            <form method="POST" class="card-body" action="{{ route('company-admin.home.update.company') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label>Company Name</label>
                                        <input id="company_name" type="text" class="form-control @error('company_name') is-invalid @enderror" name="company_name" value="{{ $company->name }}" required autocomplete="company_name" autofocus>

{{--                                        <input class="form-control" type="text" name="company_name" value="{{$company->name}}">--}}
                                        @error('company_name')
                                        <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Company Address</label>
                                        <input id="company_address" type="text" class="form-control @error('company_address') is-invalid @enderror" name="company_address" value="{{ $company->address }}" required autocomplete="company_address" autofocus>

                                        {{--                                        <input class="form-control" type="text" name="company_address" value="{{$company->address}}">--}}
                                        @error('company_address')
                                        <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                        @enderror
                                    </div>
                                    <button type="submit" class="btn btn-info">Update Company</button>
                            </form>
                        </div>
                        <div class="col-6 card-body">
                            <img src="{{ asset('img/admin/undraw_product_tour_re_8bai.svg') }}" width="50%" style="display:block;margin: auto">
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection
