@extends('users.layouts.layout')

@section('content')
    <div class="main-body">
        <!-- /Breadcrumb -->
        @if(session()->has('message'))
{{--            <div class="alert alert-success alert-dismissible" role="alert">--}}
{{--                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>--}}
{{--                {{ session()->get('message') }}--}}
{{--            </div>--}}
            <div id="success-alert" class="alert alert-success alert-dismissible" role="alert" style="position: fixed;right: 7px;width: 86%;z-index: 2;color: #155724;background-color: #d4edda;border-color: #c3e6cb;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="card">
            <div class="row justify-content-center">
                <div class="col-md-10">


                    <div class="row justify-content-center">
                        <div class="col-md-6 mb-3">
                            <p><img src="{{ asset('img/admin/undraw_message_sent_re_q2kl.svg') }}" alt="Image" class="col-8 img-fluid"></p>


                        </div>
                        <div class="col-md-6 h-100 m-auto">

                            <form class="p-3" method="post" id="contactForm" name="contactForm" action="{{ route('company-admin.user.invitation') }}">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email" required autocomplete="email" autofocus>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <input type="submit" value="Send Invitation" class="btn btn-info">
                                        <span class="submitting"></span>
                                    </div>
                                </div>
                            </form>

                            <div id="form-message-warning mt-4"></div>
{{--                            <div id="form-message-success">--}}
{{--                                Your message was sent, thank you!--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
