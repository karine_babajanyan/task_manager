@extends('users.layouts.layout')

@section('content')
    <div class="main-body">
        <!-- /Breadcrumb -->

            <div class="card">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Update</th>
                            <th scope="col">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $key=>$user)
                            <tr>
                                <th scope="row">{{$user->id}}</th>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td><button type="button" class="btn btn-warning"><i class="fas fa-edit"></i></button></td>
                                <td><button type="button" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
    </div>
@endsection
