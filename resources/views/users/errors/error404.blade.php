<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div style="width: 75%; margin: auto">
        <img src="{{asset('/img/errors/undraw_page_not_found_re_e9o6.svg')}}" width="100%">
    </div>
</body>
</html>
