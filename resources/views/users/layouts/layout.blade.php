<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="{{ asset('css/admin/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('css/admin/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/admin/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin/change-profile.css') }}">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

    <!-- Preloader -->
    <div class="preloader flex-column justify-content-center align-items-center">
        <img class="animation__shake" src="{{ asset('img/admin/AdminLTELogo.png') }}" alt="AdminLTELogo" height="60" width="60">
    </div>

    <!-- Navbar -->
    <nav class="main-header navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item p-2">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item dropdown ml-auto p-2">
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">

        <a href="/home" class="brand-link">
            <img src="{{ asset('img/admin/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">Admin Panel</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    @if(Auth::user()->profile_picture ==null)
                        <img src='https://cdn1.iconfinder.com/data/icons/random-115/24/person-512.png' class="elevation-2" alt="User Image">
                    @else
                        <img src='{{asset("uploads/" . Auth::user()->profile_picture)}}' class="elevation-2" alt="User Image" />
                    @endif
                </div>
                <div class="info">
                    <a href="/home" class="d-block">{{ Auth::user()->name }}</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
{{--                    <li class="nav-header">Profile</li>--}}
                            @if(Auth::user()->isCompanyAdmin())
                                <li class="nav-item">
                                    <a href="#" class="nav-link {{  request()->routeIs('company-admin.home.*') ? 'active' : '' }}">
                                        {{--                            <i class="nav-icon fas fa-user-edit"></i>--}}
                                        <i class="nav-icon fas fa-user"></i>
                                        <p>
                                            Profile
                                            <i class="fas fa-angle-left right"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <a href="/company-admin/home" class="nav-link {{  request()->routeIs('company-admin.home.show') ? 'active' : '' }}">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Show</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="/company-admin/home/edit/profile" class="nav-link {{  request()->routeIs('company-admin.home.edit') ? 'active' : '' }}">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Edit</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link {{  request()->routeIs('company-admin.users.*') ? 'active' : '' }}">
                                        <i class="nav-icon fas fa-users"></i>
                                        <p>
                                            Users
                                            <i class="fas fa-angle-left right"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <a href="/company-admin/users" class="nav-link {{  request()->routeIs('company-admin.users.update') ? 'active' : '' }}">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Show</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="/company-admin/user/create" class="nav-link {{  request()->routeIs('company-admin.users.create') ? 'active' : '' }}">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Create</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            @endif

                            @if(Auth::user()->isAdmin())
                                <li class="nav-item">
                                    <a href="#" class="nav-link {{  request()->routeIs('company-admin.home.*') ? 'active' : '' }}">
                                        <i class="nav-icon fas fa-user"></i>
                                        <p>
                                            Profile
                                            <i class="fas fa-angle-left right"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <a href="/admin/home" class="nav-link {{  request()->routeIs('admin.home.show') ? 'active' : '' }}">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Show</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="/admin/home/edit/profile" class="nav-link {{  request()->routeIs('admin.home.edit') ? 'active' : '' }}">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Edit</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            @endif
{{--                        <a href="" class="nav-link active">--}}
{{--                            <i class="nav-icon fas fa-user-edit"></i>--}}
{{--                            <i class="nav-icon fas fa-edit"></i>--}}
{{--                            <p>--}}
{{--                                Edit Profile--}}
{{--                            </p>--}}
{{--                        </a>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a href="#" class="nav-link">--}}
{{--                            <i class="nav-icon fas fa-book"></i>--}}
{{--                            <p>--}}
{{--                                Pages--}}
{{--                                <i class="fas fa-angle-left right"></i>--}}
{{--                            </p>--}}
{{--                        </a>--}}
{{--                        <ul class="nav nav-treeview">--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="pages/examples/invoice.html" class="nav-link">--}}
{{--                                    <i class="far fa-circle nav-icon"></i>--}}
{{--                                    <p>Invoice</p>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}
{{--                    <li class="nav-header">MULTI LEVEL EXAMPLE</li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a href="#" class="nav-link">--}}
{{--                            <i class="nav-icon fas fa-circle"></i>--}}
{{--                            <p>--}}
{{--                                Level 1--}}
{{--                                <i class="right fas fa-angle-left"></i>--}}
{{--                            </p>--}}
{{--                        </a>--}}
{{--                        <ul class="nav nav-treeview">--}}
{{--                            <li class="nav-item">--}}
{{--                                <a href="#" class="nav-link">--}}
{{--                                    <i class="far fa-circle nav-icon"></i>--}}
{{--                                    <p>--}}
{{--                                        Level 2--}}
{{--                                        <i class="right fas fa-angle-left"></i>--}}
{{--                                    </p>--}}
{{--                                </a>--}}
{{--                                <ul class="nav nav-treeview">--}}
{{--                                    <li class="nav-item">--}}
{{--                                        <a href="#" class="nav-link">--}}
{{--                                            <i class="far fa-dot-circle nav-icon"></i>--}}
{{--                                            <p>Level 3</p>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>
    <div class="container-fluid">
    @yield('content')
    </div>
</div>
<!-- jQuery -->
<script src="{{ asset('js/admin/plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('js/admin/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('js/admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('js/admin/change-profile.js') }}" defer></script>
<script src="{{ asset('js/admin/adminlte.min.js') }}" defer></script>
<script src="{{ asset('js/admin/demo.js') }}" defer></script>
<script>
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
    });
</script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{--<script src="{{ asset('js/admin/pages/dashboard.js') }}"></script>--}}
{{--<!-- Summernote -->--}}
{{--<script src="{{ asset('js/admin/plugins/summernote/summernote-bs4.min.js') }}"></script>--}}
</body>
</html>
