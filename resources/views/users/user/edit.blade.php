@extends('layouts.app')
@section('styles')
    <link href="{{ asset('css/user/profile.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="container">
        <div class="wrapper bg-white mt-sm-5">
            <h4 class="pb-4 border-bottom">Account settings</h4>
            <div class="py-2">
                <div class="row border-bottom">
                    <div class="col-6">
                        <form lass="card-body" method="POST" action="{{ route('user.update.profile') }}" enctype="multipart/form-data">
                            @csrf
{{--                            <div class="d-flex align-items-start py-3 border-bottom">--}}
{{--                                <img src="https://sgame.dit.upm.es/pictures/12946.png?1608547866/" class="img" alt="">--}}
{{--                                <div class="pl-sm-4 pl-2" id="img-section">--}}
{{--                                    <p><b>Profile Photo</b></p>--}}
{{--                                    <input type="file" id="upload" hidden/>--}}
{{--                                    <label for="upload" class="btn button border">Choose file</label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="col-12 mb-3">
                                <div class="avatar-wrapper @error('file') is-invalid @enderror">
                                    @if($user->profile_picture ==null)
                                        <img class="profile-pic" src="https://cdn1.iconfinder.com/data/icons/random-115/24/person-512.png" />
                                    @else
                                        <img class="profile-pic" src='{{asset("uploads/$user->profile_picture")}}' />
                                    @endif
                                    <div class="upload-button">
                                        <i class="fas fa-arrow-circle-up"></i>
                                    </div>
                                    {{--                                        <input id="file" type="file" class="file-upload" name="file">--}}
                                    <input type="file" name="file" class="form-control file-upload" id="file">
                                </div>
                                @error('file')
                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Name</label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" required autocomplete="name" autofocus>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" required autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                {{--                        <button class="btn btn-info" type="submit">Update</button>--}}
                                <div class="py-3 pb-4">
                                    <button class="btn btn-primary mr-3">Update Profile</button>
{{--                                    <button class="btn btn-light">Cancel</button> --}}
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-6">
                        <div class="card-body">
                            <h4 class="text-white text-white bg-dark d-inline-block p-1 rounded m-0"><strong>Change</strong> Password</h4>
                            <form class="card-body" method="POST" action="{{ route('user.update.password') }}">
                                @csrf
                                <div class="form-group">
                                    <label>Current Password</label>
                                    <input id="old_password" type="password" class="form-control @error('old_password') is-invalid @enderror" name="old_password" required autocomplete="old_password" autofocus>

                                    {{--                                    <input class="form-control @error('old_password') is-invalid @enderror" type="password" name="old_password" placeholder="••••••">--}}
                                    @error('old_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>New Password</label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="password" autofocus>
                                    {{--                                    <input class="form-control @error('password') is-invalid @enderror" type="password" name="password" placeholder="••••••">--}}
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Confirm <span class="d-none d-xl-inline">Password</span></label>
                                    <input id="confirm_password" type="password" class="form-control @error('confirm_password') is-invalid @enderror" name="confirm_password" required autocomplete="confirm_password" autofocus>
                                    {{--                                    <input class="form-control @error('confirm_password') is-invalid @enderror" type="password" name="confirm_password" placeholder="••••••">--}}
                                    @error('confirm_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary mr-3" type="submit">Update Password</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-12 text-center">
                        <a href="/user" type="button" class="btn btn-link">Go back</a>
                    </div>
                </div>

                <div class="d-sm-flex align-items-center pt-3" id="deactivate">
                    <div> <b>Deactivate your account</b>
                        <p>Details about your company account and password</p>
                    </div>
                    <div class="ml-auto"> <button class="btn btn-danger">Deactivate</button> </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/user/main.js') }}" defer></script>
@endsection

