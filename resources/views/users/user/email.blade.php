<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Email</title>
</head>
<body>
<div class=e102_47 style="width:100%;margin: auto;background-color: rgba(255, 255, 255, 1);">
    <div class="e102_1" style="margin-top: 50px;margin-bottom: 20px;width: 100%;display: inline-block;background-color: #8CCDB0;height: 4px;"></div>
    <div class=e102_9 style="width:55%;margin: auto;">
        <img src="https://icon-library.com/images/green-email-icon/green-email-icon-23.jpg" width="100%">
    </div>
    <div  class="e102_39" style="color:rgba(0, 128.0000075697899, 55.00000052154064, 1);width:100%;display: inline-block;margin-top: 20px;font-family:Roboto;text-align:center;font-size:36px;letter-spacing:0;">Confirme Invitation Email</div>
    <div  class="e102_40" style="color:rgba(0, 128.0000075697899, 55.00000052154064, 1);padding: 20px;font-family:Roboto;text-align:center;font-size:18px;letter-spacing:0;">This email was sent to confirm the invitation. If you sent, then confirm, if not, do not pay attention.</div>
    <div  class="e102_44_1" style="text-align: center">
        <a href="{{ route('user.registration.show', ['email'=>$email, 'code'=>$code, 'hash_code'=>md5($code)]) }}" class="e102_44" style="background-color:rgba(0, 128.0000075697899, 55.00000052154064, 1);display:inline-block;margin-bottom:0;font-weight:400;text-align:center;white-space:nowrap;vertical-align:middle;-ms-touch-action:manipulation;touch-action:manipulation;cursor:pointer;background-image:none;border:1px solid transparent;padding:12px 30px;font-size:18px;line-height:1.42857143;border-radius:4px;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;color: white;">Confirm Invitation</a>
    </div>
    <div class="e102_2" style="margin-top: 20px;margin-bottom: 50px;width: 100%;display: inline-block;background-color: #8CCDB0;height: 4px;"></div>
</div>
</body>
</html>
