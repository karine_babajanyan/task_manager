<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign Up Form by Colorlib</title>

    <!-- Font Icon -->
{{--    <link rel="stylesheet" href="{{ asset('fonts/user/material-icon/css/material-design-iconic-font.min.css') }}">--}}
    <link rel="stylesheet" href="{{ asset('css/user/plugins/fontawesome_pro_v6/css/all.css') }}">

    <!-- Main css -->
    <link rel="stylesheet" href="{{ asset('css/user/style.css') }}">
</head>
<body>

<div class="main">
        <div class="container">
            <div class="signup-content">
                <div class="signup-image">
                    <figure><img src="{{ asset('img/user/undraw_designer_re_5v95.svg') }}" alt="sing up image" width="100%"></figure>
                </div>
                <div class="signup-form">
                    <h2 class="form-title">Sign up</h2>
                    <form method="POST" class="register-form" id="register-form" action="{{route('user.registration.create', ['code'=>$code, 'hash_code'=>md5($code)])}}">
                        @csrf
                        <div class="form-group">
                            <label for="name"><i class="fas fa-user"></i></label>
{{--                            <input type="text" name="name" id="name" placeholder="Name"/>--}}
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Name">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="email"><i class="fas fa-envelope"></i></label>
{{--                            <input type="email" name="email" id="email" placeholder="Email" value="{{$email}}"/>--}}
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$email}}" required autocomplete="email" placeholder="Email">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password"><i class="fas fa-lock"></i></label>
{{--                            <input type="password" name="password" id="password" placeholder="Password"/>--}}
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password-confirm"><i class="far fa-lock"></i></label>
{{--                            <input type="password" name="confirm_password" id="confirm_password" placeholder="Repeat password"/>--}}
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Repeat password">

                        </div>
                        <div class="form-group form-button">
                            <input type="submit" name="signup" id="signup" class="form-submit" value="Register"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</div>

<!-- JS -->
<script src="{{ asset('js/user/jquery/jquery.min.js') }}"></script>
{{--<script src="js/main.js"></script>--}}
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
