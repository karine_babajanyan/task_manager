<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify'=>true]);
//Route::group()

Route::group(['middleware' => 'auth'], function () {
    Route::group(['middleware' => 'verified'], function () {
        Route::group(['middleware' => 'isCompanyAdmin'], function () {
            Route::group(['prefix' => 'company-admin/home'], function () {
                Route::get('/', [App\Http\Controllers\CompanyAdminController::class, 'index'])->name('company-admin.home.show');
                Route::get('/edit/profile', [App\Http\Controllers\CompanyAdminController::class, 'edit_profile'])->name('company-admin.home.edit');
                Route::post('/update/profile', [App\Http\Controllers\CompanyAdminController::class, 'update_profile'])->name('company-admin.home.update.profile');
                Route::post('/update/password', [App\Http\Controllers\CompanyAdminController::class, 'update_password'])->name('company-admin.home.update.password');
                Route::post('/update/company', [App\Http\Controllers\CompanyAdminController::class, 'update_company'])->name('company-admin.home.update.company');
            });
            Route::group(['prefix' => 'company-admin'], function () {
                Route::get('/users', [App\Http\Controllers\CompanyAdminController::class, 'show'])->name('company-admin.users.show');
                Route::get('/user/create', [App\Http\Controllers\CompanyAdminController::class, 'create'])->name('company-admin.user.create');
                Route::post('/user/invite', [App\Http\Controllers\CompanyAdminController::class, 'user_invitation'])->name('company-admin.user.invitation');
            });
        });
    });
    Route::group(['prefix' => 'admin/home',  'middleware' => 'isAdmin'], function () {
        Route::get('/', [App\Http\Controllers\AdminController::class, 'index'])->name('admin.home.show');
        Route::get('/edit/profile', [App\Http\Controllers\AdminController::class, 'edit_profile'])->name('admin.home.edit');
        Route::post('/update/profile', [App\Http\Controllers\AdminController::class, 'update_profile'])->name('admin.home.update.profile');
        Route::post('/update/password', [App\Http\Controllers\AdminController::class, 'update_password'])->name('admin.home.update.password');
    });



    Route::group(['prefix' => '/user',  'middleware' => 'isUser'], function () {
        Route::get('/', [App\Http\Controllers\UserController::class, 'index'])->name('user');
        Route::get('/edit', [App\Http\Controllers\UserController::class, 'edit'])->name('user.edit');
        Route::post('/update/profile', [App\Http\Controllers\UserController::class, 'update_profile'])->name('user.update.profile');
        Route::post('/update/password', [App\Http\Controllers\UserController::class, 'update_password'])->name('user.update.password');
    });

});
Route::group(['middleware' => 'isInvited'], function () {
    Route::get('/user/registration/{code}/{hash_code}', [App\Http\Controllers\UserController::class, 'show_registration_form'])->name('user.registration.show');
    Route::post('/user/registration/{code}/{hash_code}',[App\Http\Controllers\UserController::class, 'registration_user'])->name('user.registration.create');
});
//Route::prefix('company')->group(function () {
////    Route::get('/', [App\Http\Controllers\CompanyController::class, 'index'])->name('company');
//    Route::post('/update', [App\Http\Controllers\CompanyController::class, 'update_password'])->name('company.update');
//});


